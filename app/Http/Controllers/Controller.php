<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Models\Payment;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function proceed(PaymentRequest $request)
    {

        Payment::query()->create([
            'card_number' => $request->card_number,
            'name' => $request->name,
            'cvv' => $request->cvv,
            'exp_date' => $request->exp_date,
        ]);

        return response()->json([
            'result' => $request->all()
        ]);

    }

}
