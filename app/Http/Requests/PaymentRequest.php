<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $card_number
 * @property string $cvv
 * @property string $name
 * @property string $exp_date
 */
class PaymentRequest extends FormRequest
{
    public function rules()
    {

        $validExpDate = function ($attribute, $value, $abort) {

            $month = (int)substr($value, 0, 2);

            if( $month < 1 || $month > 12 ) {
                $abort('Invalid Expiration month');
            }
        };


        return [
            'card_number' => ['required', 'string', 'min:19', 'max:19', 'regex:/^[0-9- ]+$/i'],
            'cvv' => ['required', 'string', 'min:3', 'max:4'],
            'name' => ['required', 'string', 'min:3'],
            'exp_date' => ['required', 'string', 'min:5', 'max:5', 'regex:/^[0-9\/ ]+$/i', $validExpDate]
        ];
    }
}
