<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Localization
Route::get('locale/{lang}', [\App\Http\Controllers\LocalizationController::class, 'setLang']);

Route::get('/', function () {
    return view('welcome', [
        'page' => 'Home'
    ]);
})->name('home');


Route::get('/cart', function () {
    return view('cart', [
        'page' => 'Cart'
    ]);
})->name('cart');


Route::get('/store', function() {
    return view('store', [
        'page' => 'Store'
    ]);
})->name('store');

Route::get('/liked', function() {
    return view('liked', [
        'page' => 'Liked'
    ]);
})->name('liked');

Route::get('/payment', function () {
    return view('payment', [
        'page' => 'payment'
    ]);
})->name('payment');


Route::post('/make_payment', [Controller::class, 'proceed'])
    ->name('make-payment');


Route::get('/iphone_gold', function () {
    return view('iphone_gold', [
        'page' => 'iphone_gold'
    ]);
})->name('iphone_black');

Route::get('/iphone_black', function () {
    return view('iphone_black', [
        'page' => 'iphone_black'
    ]);
})->name('iphone_white');

Route::get('/iphone_white', function () {
    return view('iphone_white', [
        'page' => 'iphone_white'
    ]);
})->name('iphone_white');

Route::get('/buds_bronze', function () {
    return view('buds_bronze', [
        'page' => 'buds_bronze'
    ]);
})->name('buds_bronze');

Route::get('/buds_white', function () {
    return view('buds_white', [
        'page' => 'buds_white'
    ]);
})->name('buds_white');

Route::get('/buds_black', function () {
    return view('buds_black', [
        'page' => 'buds_black'
    ]);
})->name('buds_black');

Route::get('/airpods_white', function () {
    return view('airpods_white', [
        'page' => 'airpods_white'
    ]);
})->name('airpods_white');

Route::get('/airpods_red', function () {
    return view('airpods_red', [
        'page' => 'airpods_red'
    ]);
})->name('airpods_red');

Route::get('/airpods_black', function () {
    return view('airpods_black', [
        'page' => 'airpods_black'
    ]);
})->name('airpods_black');

Route::get('/flip_lavender', function () {
    return view('flip_lavender', [
        'page' => 'flip_lavender'
    ]);
})->name('flip_lavender');

Route::get('/flip_black', function () {
    return view('flip_black', [
        'page' => 'flip_black'
    ]);
})->name('flip_black');


Route::get('/flip_gold', function () {
    return view('flip_gold', [
        'page' => 'flip_gold'
    ]);
})->name('flip_gold');

Route::get('/awatch_white', function () {
    return view('awatch_white', [
        'page' => 'awatch_white'
    ]);
})->name('awatch_white');

Route::get('/awatch_red', function () {
    return view('awatch_red', [
        'page' => 'awatch_red'
    ]);
})->name('awatch_red');

Route::get('/awatch_black', function () {
    return view('awatch_black', [
        'page' => 'awatch_black'
    ]);
})->name('awatch_black');


Route::get('/gwatch_white', function () {
    return view('gwatch_white', [
        'page' => 'gwatch_white'
    ]);
})->name('gwatch_white');

Route::get('/gwatch_blue', function () {
    return view('gwatch_blue', [
        'page' => 'gwatch_blue'
    ]);
})->name('gwatch_blue');

Route::get('/gwatch_gray', function () {
    return view('gwatch_gray', [
        'page' => 'gwatch_gray'
    ]);
})->name('gwatch_gray');

Route::get('/iphone11pro', function () {
    return view('iphone11pro', [
        'page' => 'iphone11pro'
    ]);
})->name('iphone11pro');

Route::get('/iphone12pro', function () {
    return view('iphone12pro', [
        'page' => 'iphone12pro'
    ]);
})->name('iphone12pro');

Route::get('/iphone13pro', function () {
    return view('iphone13pro', [
        'page' => 'iphone13pro'
    ]);
})->name('iphone13pro');


Route::get('/iphone14_black', function () {
    return view('iphone14_black', [
        'page' => 'iphone14_black'
    ]);
})->name('iphone14_black');

Route::get('/iphone14_red', function () {
    return view('iphone14_red', [
        'page' => 'iphone14_red'
    ]);
})->name('iphone14_red');

Route::get('/iphone14_white', function () {
    return view('iphone14_white', [
        'page' => 'iphone14_white'
    ]);
})->name('iphone14_white');



