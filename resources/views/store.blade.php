<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <x-desktop.new_store></x-desktop.new_store>
        </section>
    </section>

    <section class="mobile_interface">
        <x-mobile.store_mobile></x-mobile.store_mobile>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/price_range.css">
        <link rel="stylesheet" href="css/for_checkboxes.css">
        <link rel="stylesheet" href="css/new_store.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/store_things.css">
        <link rel="stylesheet" href="css/mobile/store_mobile.css">
        <link rel="stylesheet" href="css/for_color_change.css">
    @endpush
</x-layouts.base>

