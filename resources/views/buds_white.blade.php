<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>Samsung Galaxy Buds Live Bronze</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/buds_white.png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/buds_white.png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/buds_white1 (1).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/buds_white1 (2).png" onclick = replace('3') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "4" src="img/for_store/buds_white1 (3).png" onclick = replace('4') alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <div class="colour_buttons">
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="buds_white"  style=" box-shadow: 0 0 10px gray; background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                                        </a>
                                        <a href="buds_bronze" style =" border: 1px solid var(--the_pink); background-color: var(--the_pink);" class="colour_button button2">

                                        </a>
                                        <a  href="buds_black" style = "border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                                        </a>
                                    </div>

                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Appointment</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Ordinary</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Type of construction</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Wire-free (TVS)</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Mounting type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Without mounting</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Connection type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Wireless</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Microphone</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Built-in</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>73.99$</h5>
                        </div>
                        <div>
                            {{__('date_pickup')}}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From 73.99$ or $24.66/mo. for 3 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('buds_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>Samsung Galaxy Buds Live White</h5>
                </div>

                <div class="the_middle">
                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/buds_white.png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/buds_white.png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/buds_white1 (1).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/buds_white1 (2).png" onclick = replace('7') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="8" src="img/for_store/buds_white1 (3).png" onclick = replace('8') alt="">
                            </div>
                        </div>
                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div class="colour_buttons">
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="buds_white"  style=" box-shadow: 0 0 10px gray; background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                            </a>
                            <a href="buds_bronze" style ="  border: 1px solid var(--the_pink); background-color: var(--the_pink);" class="colour_button button2">

                            </a>
                            <a  href="buds_black" style = " border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                            </a>
                        </div>

                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Appointment</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Ordinary</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Type of construction</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Wire-free (TVS)</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Mounting type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Without mounting</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Connection type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Wireless</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Microphone</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Built-in</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>73.99$</h5>
                </div>
                <div>
                    {{__('date_pickup')}}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From 73.99$ or $24.66/mo. for 3 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

