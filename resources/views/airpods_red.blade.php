<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>Air pods Max White</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/Air_pods_red.png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/Air_pods_red.png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/airpods_red1 (1).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/airpods_red1 (2).png" onclick = replace('3') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "4" src="img/for_store/airpods_red1 (3).png" onclick = replace('4') alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <div class="colour_buttons">
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="airpods_white"  style=" background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                                        </a>
                                        <a href="airpods_red" style =" box-shadow: 0 0 10px gray; border: 1px solid #DB3044; background-color: #DB3044;" class="colour_button button2">

                                        </a>
                                        <a  href="airpods_black" style = "border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                                        </a>
                                    </div>

                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Appointment</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Ordinary</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Type of construction</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Full-size</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Mounting type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>With a headband</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Connection type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Wired+Wireless</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Microphone</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Built-in</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>425.99$</h5>
                        </div>
                        <div>
                            {{__('date_pickup')}}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From 425.99$ or $17.76/mo. for 24 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('airpods_max_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
        <x-desktop.end_info></x-desktop.end_info>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>Air pods Max Red</h5>
                </div>

                <div class="the_middle">

                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/Air_pods_red.png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/Air_pods_red.png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/airpods_red1 (1).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/airpods_red1 (2).png" onclick = replace('7') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="8" src="img/for_store/airpods_red1 (3).png" onclick = replace('8') alt="">
                            </div>
                        </div>
                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div class="colour_buttons">
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="airpods_white"  style=" background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                            </a>
                            <a href="airpods_red" style ="box-shadow: 0 0 10px gray; border: 1px solid #DB3044; background-color: #DB3044;" class="colour_button button2">

                            </a>
                            <a  href="airpods_black" style = "border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                            </a>
                        </div>

                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Appointment</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Ordinary</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Type of construction</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Full-size</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Mounting type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>With a headband</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Connection type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Wired+Wireless</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Microphone</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Built-in</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>425.99$</h5>
                </div>
                <div>
                    {{__('date_pickup')}}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From 425.99$ or $17.76/mo. for 24 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

