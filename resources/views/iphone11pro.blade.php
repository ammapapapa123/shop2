<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>Apple iPhone 11 Pro</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/iphone11pro(3).png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/iphone11pro(3).png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/iphone11pro(4).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/iphone11pro(5).png" onclick = replace('3') alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="description">
                                <div >
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="iphone11pro"  style="box-shadow: 0 0 10px gray; background-color: gray; border: 1px solid gray;"    class="colour_button button1">
                                        </a>
                                    </div>
                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Model year</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>2022</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Screen</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>5.8"/2436x1125 Pixels</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Screen Technology</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>OLED</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Processor type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>A13 Bionic</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Built-in memory (ROM)</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>256 GB</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Main Camera Megapixel</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>12/12/12</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Video resolution</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>3840x2160 Pixels (4K)</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Optical stabilization </p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Yes</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Optical zoom for magnification</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>x2</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>$247.89</h5>
                        </div>
                        <div>
                            {{ __('date_pickup') }}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From $247.89 or $10.32/mo. for 24 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('iphone11Pro_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>Apple iPhone 11 Pro</h5>
                </div>

                <div class="the_middle">
                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/iphone11pro(3).png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/iphone11pro(3).png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/iphone11pro(4).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/iphone11pro(5).png" onclick = replace('7') alt="">
                            </div>
                        </div>

                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div >
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="iphone11pro"  style="box-shadow: 0 0 10px gray; background-color: gray; border: 1px solid gray;"    class="colour_button button1">
                            </a>
                        </div>
                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Model year</p>
                                </div>
                                <div style="text-align: right">
                                    <p>2022</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Screen</p>
                                </div>
                                <div style="text-align: right">
                                    <p>5.8"/2436x1125 Pixels</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Screen Technology</p>
                                </div>
                                <div style="text-align: right">
                                    <p>OLED</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Processor type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>A13 Bionic</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Built-in memory (ROM)</p>
                                </div>
                                <div style="text-align: right">
                                    <p>256 GB</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Main Camera Megapixel</p>
                                </div>
                                <div style="text-align: right">
                                    <p>12/12/12</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Video resolution</p>
                                </div>
                                <div style="text-align: right">
                                    <p>3840x2160 Pixels (4K)</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Optical stabilization </p>
                                </div>
                                <div style="text-align: right">
                                    <p>Yes</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Optical zoom for magnification</p>
                                </div>
                                <div style="text-align: right">
                                    <p>x2</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>$247.89</h5>
                </div>
                <div>
                    {{ __('date_pickup') }}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From $247.89 or $10.32/mo. for 24 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

