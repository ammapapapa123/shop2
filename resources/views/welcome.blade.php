<x-layouts.base :page="$page">


    <div class="desktop_interface">
        <section class="background">
            <div class="on_top_image">
                <img src="img/ie_galaxy_buds_live_r180_sm_r180nznaeua_frontmysticbronze_296154303.png" alt="">
            </div>
            <x-desktop.home></x-desktop.home>
        </section>
        <x-desktop.phones_section></x-desktop.phones_section>
        <x-desktop.watch_section></x-desktop.watch_section>
        <x-desktop.footer></x-desktop.footer>

    </div>

    <div class="mobile_interface">
        <x-mobile.home_mobile></x-mobile.home_mobile>
    </div>

    @push('head')
        <link rel="stylesheet" href="css/home.css">
        <link rel="stylesheet" href="css/phones_section.css">
        <link rel="stylesheet" href="css/watch_section.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/mobile/home_mobile.css">
        <link rel="stylesheet" href="css/end_info.css">
    @endpush

</x-layouts.base>
