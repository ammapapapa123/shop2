<section class="home for_padding">
    <div style="align-content: center">
        <img class= "samsung_picture" src="img/black-samsung-logo-png-21.png" alt="">
        <img style="width: 530px" src="img/TMOM66291-frontimage.png" class ="one" alt="">
    </div>

    <div class="home_text">
        <h1 style="color: #4E4D4D;">
            Headphones <br>
            <span style="color:#AD7067">true</span> samsung
        </h1>
        <h5 style="color: #4E4D4D;">
            {{ __('header1.1') }} <br>
            {{ __('header1.2') }} <br>
            {{ __('header1.3') }}
        </h5>
        <div>
            <a  class="btn btn-secondary" href="store" role="button">{{ __('buy_now') }}</a>
        </div>
    </div>
</section>
<h1 style="color:white" class="background_text">
    Air <br>
    Pods<br>
    Max
</h1>
<section class = "home for_padding">
    <div>
        <img  style="width: 530px" src="img/hero__gnfk5g59t0qe_xlarge.png" class ="one" alt="">
    </div>

    <div class="home_text">
        <h1 style="color: #494949;">
            Air <br>
            Pods<br>
            Max
        </h1>
        <h5 style= "color: #494949">
            {{ __('header2.1') }} <br>
            {{ __('header2.2') }} <br>
            {{ __('header2.3') }} <br>
            {{ __('header2.4') }}
        </h5>
        <div>
            <a  class="btn btn-dark" href="store" role="button">Store</a>
        </div>
    </div>
</section>
