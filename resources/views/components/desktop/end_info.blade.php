<section id ="end" class="info_end desktop_interface" >
    <div class="row_info">
        <div class="c1_info">
            <div>
                <p style="text-transform: capitalize;">
                    {{__('shipping')}} <br>
                    {{__('twelve')}} <br>
                </p>
                <div class="d-flex">
                    <img src="" alt="">
                </div>
            </div>
            <div>
                <form class="row g-3">
                    <div class="col-auto">
                        <label for="inputPassword2" class="visually-hidden">Password</label>
                        <input type="password" class="form-control" id="inputPassword2" placeholder="E-Mail">
                    </div>
                    <div class="col-auto">
                        <a href ="#end" class="btn btn-light mb-3">Submit</a>
                    </div>
                </form>
            </div>
            <div class="d-flex gap-4 align-items-center">
                    <div>
                        <p>{{__('partners')}}</p>
                    </div>
            </div>
            <div class="d-flex">
                <img style="width: 100px" src="img/icons/1.png" alt="">
                <img style="width: 100px" src="img/icons/2.png" alt="">
            </div>
        </div>

        <div>
            <div>
                <h5>{{__('page_m')}}</h5>
            </div>
            <div>
                <p class="page_m">
                    <a style="text-decoration: none" href="">
                        {{__('home')}}<br>
                    </a>
                    <a style="text-decoration: none" href="store">
                        {{__('store')}}<br>
                    </a>
                </p>
            </div>

            <div>
                <p>
                    {{__('one')}} <br>
                    {{__('two')}}
                </p>
            </div>
        </div>



        <div style="width: 300px">
            <h5>{{__('office')}}</h5>
            <x-desktop.map></x-desktop.map>
        </div>

    </div>
    <div class="d-flex gap-5">
        <div>
            <p>
                {{__('header4.4')}}
            </p>
        </div>
        <div>
            {{__('header4.5')}}
        </div>
    </div>
</section>
