<section class="new_store">
    <div class="outline">
        <div class="choose_options">
            <div class="search">
                <div class="mb-3">
                    <input type="search" class="form-control" id="search" placeholder="Search">
                </div>
            </div>
            <div class="categories">
                <p>{{__('categories')}}</p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="all"  onclick="categories('all')" checked>
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('all')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="headphones" onclick="categories('headphones')">
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('headphones')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="phones" onclick="categories('phones')">
                    <label class="form-check-label" for="flexRadioDefault2">
                        {{__('phones')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="watch" onclick="categories('watch')">
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('watch')}}
                    </label>
                </div>
            </div>
            <div>
                <p>{{__('range')}}</p>
                <x-price_range></x-price_range>
            </div>
            <div class="filter">
                <p >{{__('filter')}}</p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        {{__('discount')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                    <label class="form-check-label" for="flexCheckChecked">
                        {{__('available')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                    <label class="form-check-label" for="flexCheckChecked">
                        {{__('d_available')}}
                    </label>
                </div>
            </div>
            <div style="margin-top: 20px">
                <a href = "cart" type="button" class="btn btn-light">{{__('cart')}} </a>
            </div>
        </div>


        <div class="product_cards">
            <div class="p_card">


                <div class="card" id="14black_card">
                    <div class="img_card slide1">
                        <a href="iphone14_black">
                            <img src="img/for_store/iphone14black (2).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Black</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_black');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_black" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="14red_card">
                    <div class="img_card slide1">
                        <a href="iphone14_red">
                            <img src="img/for_store/iphone14red (3).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Red</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_red');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_red" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="14white_card">
                    <div class="img_card slide1">
                        <a href="iphone14_white">
                            <img src="img/for_store/iphone14white (3).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 White</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_white');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_white" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="buds_card">
                    <div class="img_card slide1">
                            <a href="buds_bronze">
                                <img src="img/for_store/buds_bronze.png" class="card-img-top" alt="...">
                            </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live Bronze</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds');">
                                <a class="btn btn-dark">
                                    <i id = "buds" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="buds_black_card">
                    <div class="img_card slide1">
                        <a href="buds_black">
                            <img src="img/for_store/buds_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live Black</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds_black');">
                                <a class="btn btn-dark">
                                    <i id = "buds_black" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="buds_white_card">
                    <div class="img_card slide1">
                        <a href="buds_white">
                            <img src="img/for_store/buds_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live White</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds_white');">
                                <a class="btn btn-dark">
                                    <i id = "buds_white" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="air_pods_card">
                    <div class="img_card slide1">
                        <a href="airpods_white">
                            <img src="img/mobile/airpods.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="air_pods_black_card">
                    <div class="img_card slide1">
                        <a href="airpods_black">
                            <img src="img/for_store/Air_pods_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods_black');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods_black" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="air_pods_red_card">
                    <div class="img_card slide1">
                        <a href="airpods_red">
                            <img src="img/for_store/Air_pods_red.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods_red');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods_red" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="iphone_black_card">
                    <div class="img_card slide1">
                        <a href="iphone_black">
                            <img src="img/for_store/iphone_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max Black</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone_black');">
                                <a class="btn btn-dark">
                                    <i id="iphone_black" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="iphone_gold_card">
                    <div class="img_card slide1">
                        <a href="iphone_gold">
                            <img src="img/for_store/iphone_gold.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max Gold</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone_gold');">
                                <a class="btn btn-dark">
                                    <i id="iphone_gold" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="iphone_card">
                    <div class="img_card slide1">
                        <a href="iphone_white">
                            <img src="img/mobile/iphone.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max White</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone');">
                                <a class="btn btn-dark">
                                    <i id="iphone" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_card">
                    <div class="img_card slide1">
                        <a href="flip_black">
                            <img src="img/for_store/flip_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Gray</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip');">
                                <a class="btn btn-dark">
                                    <i id="flip" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_lavender_card">
                    <div class="img_card slide1">
                        <a href="flip_lavender">
                            <img src="img/for_store/flip_lavender.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Lavender</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip_lavender');">
                                <a class="btn btn-dark">
                                    <i id="flip_lavender" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_gold_card">
                    <div class="img_card slide1">
                        <a href="flip_gold">
                            <img src="img/for_store/flip_ gold.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Gold</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip_gold');">
                                <a class="btn btn-dark">
                                    <i id="flip_gold" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_card">
                    <div class="img_card slide1">
                        <a href="awatch_black">
                            <img src="img/for_store/apple_watch_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple Watch Black</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch');">
                                <a class="btn btn-dark">
                                    <i id ="awatch" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_red_card">
                    <div class="img_card slide1">
                        <a href="awatch_red">
                            <img src="img/for_store/apple_watch_red.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple Watch Red </h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch_red');">
                                <a class="btn btn-dark">
                                    <i id ="awatch_red" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_white_card">
                    <div class="img_card slide1">
                        <a href="awatch_white">
                            <img src="img/for_store/apple_watch_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple Watch Beige</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch_white');">
                                <a class="btn btn-dark">
                                    <i id ="awatch_white" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="gwatch_card">
                    <div class="img_card slide1">
                        <a href="gwatch_blue">
                            <img src="img/for_store/galaxy_watch_blue.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch Blue</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="gwatch_gray_card">
                    <div class="img_card slide1">
                        <a href="gwatch_gray">
                            <img src="img/for_store/galaxy_watch_gray.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch Gray</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch_gray');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch_gray" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="gwatch_white_card">
                    <div class="img_card slide1">
                        <a href="gwatch_white">
                            <img src="img/for_store/galaxy_watch_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch White</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch_white');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch_white" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="11pro_card">
                    <div class="img_card slide1">
                        <a href="iphone11pro">
                            <img src="img/for_store/iphone11pro(4).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 11 Pro Gray</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">412.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">247.89$</h6>
                            </div>
                            <div onclick="toggleClass('iphone11pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone11pro" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="12pro_card">
                    <div class="img_card slide1">
                        <a href="iphone12pro">
                            <img src="img/for_store/iphone12pro (1).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 12 Pro Gray</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">599.00$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone12pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone12pro" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="13pro_card">
                    <div class="img_card slide1">
                        <a href="iphone13pro">
                            <img src="img/for_store/iphone13pro (2).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 13 Pro Gray</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">899.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone13pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone13pro" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

@push('scripts')
<script defer>

    window.onload = function () {
        Object.keys(cart).forEach(item => {
            toggleHeart(item)
        })
    }

    function toggleClass(key){
        cart[key] = !cart[key];
        toggleHeart(key)
        changeCartNumber()
        localStorage.setItem('cart', JSON.stringify(cart))
    }

    function toggleHeart(key) {
        let elem = document.getElementById(key)
        let elem_mobile = document.getElementById(key + '_mobile')
        if(cart[key]){
            elem.classList.remove("bi-heart")
            elem_mobile.classList.remove("bi-heart")
            elem.classList.add("bi-heart-fill")
            elem_mobile.classList.add("bi-heart-fill")
        }else {
            elem.classList.add("bi-heart")
            elem_mobile.classList.add("bi-heart")
            elem.classList.remove("bi-heart-fill")
            elem_mobile.classList.remove("bi-heart-fill")
        }
    }

    function changeCartNumber() {
        let cartNum = Object.values(cart).filter((el)=> el).length
        let cart_number = document.getElementById('cart_number')
        let cart_number_mobile = document.getElementById('cart_number_mobile')
        if(cartNum === 0) {
            cart_number.style.display = "none"
            cart_number_mobile.style.display = "none"
        }
        else{
            cart_number.style.display = "block"
            cart_number_mobile.style.display = "block"
            cart_number.innerText = cartNum.toString()
            cart_number_mobile.innerText = cartNum.toString()
        }
    }

    let products = [
        {
            "id": "buds_card",
            "name": "Samsung Galaxy Buds Live ",
            "type": "headphones",
            "price": 73.99,
        },
        {
            "id": "buds_black_card",
            "name": "Samsung Galaxy Buds Live ",
            "type": "headphones",
            "price": 73.99,
        },
        {
            "id": "buds_white_card",
            "name": "Samsung Galaxy Buds Live ",
            "type": "headphones",
            "price": 73.99,
        },
        {
            "id": "air_pods_card",
            "name": "Air pods Max",
            "type": "headphones",
            "price": 425.99,
        },
        {
            "id": "air_pods_red_card",
            "name": "Air pods Max",
            "type": "headphones",
            "price": 425.99,
        },
        {
            "id": "air_pods_black_card",
            "name": "Air pods Max",
            "type": "headphones",
            "price": 425.99,
        },
        {
            "id": "iphone_card",
            "name": "Iphone 14 Pro Max",
            "type": "phones",
            "price": 989.99,
        },
        {
            "id": "iphone_black_card",
            "name": "Iphone 14 Pro Max",
            "type": "phones",
            "price": 989.99,
        },
        {
            "id": "iphone_gold_card",
            "name": "Iphone 14 Pro Max",
            "type": "phones",
            "price": 989.99,
        },
        {
            "id": "flip_card",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones",
            "price": 586.99,
        },
        {
            "id": "flip_gold_card",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones",
            "price": 586.99,
        },
        {
            "id": "flip_lavender_card",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones",
            "price": 586.99,
        },
        {
            "id": "awatch_card",
            "name": "Apple Watch",
            "type": "watch",
            "price": 339.99,

        },
        {
            "id": "awatch_red_card",
            "name": "Apple Watch",
            "type": "watch",
            "price": 339.99,

        },
        {
            "id": "awatch_white_card",
            "name": "Apple Watch",
            "type": "watch",
            "price": 339.99,

        },
        {
            "id": "gwatch_card",
            "name": "Galaxy Watch",
            "type": "watch",
            "price": 236.99,
        },
        {
            "id": "gwatch_gray_card",
            "name": "Galaxy Watch",
            "type": "watch",
            "price": 236.99,
        },
        {
            "id": "gwatch_white_card",
            "name": "Galaxy Watch",
            "type": "watch",
            "price": 236.99,
        },

        {
            "id": "11pro_card",
            "name": "Iphone 11 Pro",
            "type": "phones",
            "price": 236.99,
        },

        {
            "id": "12pro_card",
            "name": "Iphone 12 Pro",
            "type": "phones",
            "price": 236.99,
        },
        {
            "id": "13pro_card",
            "name": "Iphone 13 Pro",
            "type": "phones",
            "price": 236.99,
        },

        {
            "id": "14black_card",
            "name": "Iphone 14 Black",
            "type": "phones",
            "price": 236.99,
        },

        {
            "id": "14white_card",
            "name": "Iphone 14 White",
            "type": "phones",
            "price": 236.99,
        },

        {
            "id": "14red_card",
            "name": "Iphone 14 Red",
            "type": "phones",
            "price": 236.99,
        },

    ]


    const search_input = document.getElementById("search")

    search_input.addEventListener("input", e => {
        const value = e.target.value.toLowerCase()
        products.forEach(product => {
            const can_be_seen =  product.name.toLowerCase().includes(value)
            let element = document.getElementById(product.id)
            if(can_be_seen){
                element.classList.add('dis')
                element.classList.remove('hide')
            }else{
                element.classList.add("hide")
                element.classList.remove('dis')
            }

        })
    })

    function categories(key){
        products.forEach(ct => {
            let element = document.getElementById(ct.id)
            if(key === 'all'){
                element.classList.remove('hide')
            }else{
                if(ct.type !== key){
                    element.classList.add('hide')
                    element.classList.remove('dis')
                }else{
                    element.classList.add('dis')
                    element.classList.remove('hide')
                }
            }
        })
    }



</script>
@endpush

