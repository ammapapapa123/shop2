@props(['page'])



<header id="header">
    <ul class = "menu_bar mb-0">
        <li><a href="{{ route('home') }}" class="{{ $page == 'Home' ? 'active_page' : null }}">{{__('home')}}</a></li>
        <li><a href="{{ route('store') }}" class="{{ $page == 'Store' ? 'active_page' : null }}">{{__('store')}}</a></li>
    </ul>

    <div class = "icons_for_header">
        <a style="color: #1E1E1E;" href="{{ route('cart') }}" class="cart_menu {{ $page == 'Cart' ? 'active_page' : null }} position-relative d-flex justify-content-center align-items-center">
            <span id="cart_number" class="number_badge position-absolute translate-middle badge rounded-pill bg-danger">
                3
            </span>
            <i class="bi bi-cart2"></i>
        </a>
        <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                @switch(session('locale'))
                    @case('in')
                        <img src="/img/india.png" height="24">
                        @break
                    @default
                        <img src="/img/uk.png" height="24">
                @endswitch
            </button>
            <div class="dropdown-menu dropdown-menu-end">

                <!-- item-->
                <a href="locale/en" class="dropdown-item notify-item language" data-lang="eng">
                    <img src="/img/uk.png" class="me-1"
                         height="16"> <span class="align-middle">English</span>
                </a>
                <!-- item-->
                <a href="locale/in" class="dropdown-item notify-item language" data-lang="sp">
                    <img src="/img/india.png" class="me-1"
                         height="16"> <span class="align-middle">Hindi</span>
                </a>
            </div>
        </div>
    </div>
</header>

@push('scripts')
    <script>
        try{
            var cart = JSON.parse(localStorage.getItem('cart'))
            if(!cart) {
                setDefaultCart()
            }
        }
        catch (e) {
            setDefaultCart()
        }

        function setDefaultCart() {
            cart = {
                "iphone11pro":false,
                "iphone12pro":false,
                "iphone13pro":false,
                "iphone14_red": false,
                "iphone14_white": false,
                "iphone14_black": false,
                "air_pods" : false,
                "air_pods_black":false,
                "air_pods_red":false,
                "awatch":false,
                "awatch_red":false,
                "awatch_white":false,
                "buds":false,
                "buds_black":false,
                "buds_white":false,
                "flip":false,
                "flip_gold":false,
                "flip_lavender":false,
                "gwatch":false,
                "gwatch_gray":false,
                "gwatch_white":false,
                "iphone":false,
                "iphone_black":false,
                "iphone_gold":false
            }
            localStorage.setItem('cart', JSON.stringify(cart))
        }

    </script>
    <script defer>


        let cart_number = document.getElementById('cart_number')
        let cart_number_number = document.getElementById('cart_number_mobile')
        resetCartNumber()

        function resetCartNumber() {
            let cartNum = Object.values(cart).filter((el)=> el).length
            if(cartNum === 0) {
                cart_number.style.display = "none"
                cart_number_number.style.display = "none"
            }
            else{
                cart_number.innerText = cartNum.toString()
                cart_number_number.innerText = cartNum.toString()
            }
        }

        var header = document.getElementById('header')

        document.addEventListener("scroll", (event) => {
            let pos = window.scrollY;
            if(pos > window.innerHeight - 880) {
                header.classList.add('bg-white')
            }
            else{
                header.classList.remove('bg-white')
            }
        });
    </script>
@endpush
