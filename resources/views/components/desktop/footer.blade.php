<footer class="for_padding">
    <div class="home_text" style="width: 470px">
        <h1 style="color: #494949;">
            Discounts <br>
            in our store!
        </h1>
        <h5 style="margin-bottom: 30px; color: #494949">

            {{ __('header3.1') }} <br>
            {{ __('header3.2') }} <br>
            {{ __('header3.3') }} <br>
            {{ __('header3.4') }} <br>
            {{ __('header3.5') }} <br>
        </h5>

        <div class="footer_logos">
            <i class="bi bi-apple"></i>
            <img src="img/black-samsung-logo-png-21.png" alt="">
            <a  class="btn btn-dark" href="store" role="button">{{ __('buy_now') }}</a>
        </div>
    </div>

    <div class="side_text">
        <h1 >
            DISCOUNT <br>
            DISCOUNT <br>
            DISCOUNT <br>
        </h1>
    </div>
</footer>

