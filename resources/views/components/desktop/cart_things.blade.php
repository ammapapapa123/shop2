<section class="cart_section">
    <div class="hall_cart">

        <div class="mb-2">
            <h4>{{__('shop_cart')}}</h4>
        </div>

        <div class="d-none" id="cartContent">
            <ul class="list-group">
                <li id="buds" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/buds_bronze.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Samsung Galaxy Buds Live Truly Wireless Bronze</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds')"></button>
                        <p class="fw-bolder fs-4 mb-0">$73.99</p>
                    </div>
                </li>

                <li id="buds_black" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/buds_black.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Samsung Galaxy Buds Live Truly Wireless Black</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds_black')"></button>
                        <p class="fw-bolder fs-4 mb-0">$73.99</p>
                    </div>
                </li>

                <li id="buds_white" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/buds_white.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Samsung Galaxy Buds Live Truly Wireless White</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds_white')"></button>
                        <p class="fw-bolder fs-4 mb-0">$73.99</p>
                    </div>
                </li>


                <li id="air_pods" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/mobile/airpods.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Air pods Max</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods')"></button>
                        <p class="fw-bolder fs-4 mb-0">$429.99</p>
                    </div>
                </li>

                <li id="air_pods_red" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/Air_pods_red.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Air pods Max Red</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods_red')"></button>
                        <p class="fw-bolder fs-4 mb-0">$429.99</p>
                    </div>
                </li>

                <li id="air_pods_black" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/Air_pods_black.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Air pods Max Black</h4>
                            <span class="text-muted">{{__('headphones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods_black')"></button>
                        <p class="fw-bolder fs-4 mb-0">$429.99</p>
                    </div>
                </li>


                <li id="iphone" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/mobile/iphone.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 Pro Max White</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone')"></button>
                        <p class="fw-bolder fs-4 mb-0">$989.99</p>
                    </div>
                </li>

                <li id="iphone_gold" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone_gold.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 Pro Max Gold</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone_gold')"></button>
                        <p class="fw-bolder fs-4 mb-0">$989.99</p>
                    </div>
                </li>

                <li id="iphone_black" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone_black.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 Pro Max Black</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone_black')"></button>
                        <p class="fw-bolder fs-4 mb-0">$989.99</p>
                    </div>
                </li>



                <li id="flip" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/flip_black.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">GalaxyZ Flip4 5G Black</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip')"></button>
                        <p class="fw-bolder fs-4 mb-0">$586.99</p>
                    </div>
                </li>

                <li id="flip_gold" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/flip_ gold.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">GalaxyZ Flip4 5G Gold</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip_gold')"></button>
                        <p class="fw-bolder fs-4 mb-0">$586.99</p>
                    </div>
                </li>

                <li id="flip_lavender" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/flip_lavender.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">GalaxyZ Flip4 5G Lavender</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip_lavender')"></button>
                        <p class="fw-bolder fs-4 mb-0">$586.99</p>
                    </div>
                </li>


                <li id="awatch" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/apple_watch_black.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Apple watch 8 series Black</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch')"></button>
                        <p class="fw-bolder fs-4 mb-0">$339.99</p>
                    </div>
                </li>

                <li id="awatch_red" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/apple_watch_red.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Apple watch 8 series Red</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch_red')"></button>
                        <p class="fw-bolder fs-4 mb-0">$339.99</p>
                    </div>
                </li>

                <li id="awatch_white" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/apple_watch_white.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Apple watch 8 series White</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch_white')"></button>
                        <p class="fw-bolder fs-4 mb-0">$339.99</p>
                    </div>
                </li>



                <li id="gwatch" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/galaxy_watch_blue.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Galaxy Watch Blue</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch')"></button>
                        <p class="fw-bolder fs-4 mb-0">$236.99</p>
                    </div>
                </li>

                <li id="gwatch_gray" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/galaxy_watch_gray.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Galaxy Watch Blue Gray</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch_gray')"></button>
                        <p class="fw-bolder fs-4 mb-0">$236.99</p>
                    </div>
                </li>

                <li id="gwatch_white" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/galaxy_watch_white.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Galaxy Watch Blue White</h4>
                            <span class="text-muted">{{__('watch')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch_white')"></button>
                        <p class="fw-bolder fs-4 mb-0">$236.99</p>
                    </div>
                </li>

                <li id="iphone11pro" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone11pro(3).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 11 Pro</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone11pro')"></button>
                        <p class="fw-bolder fs-4 mb-0">$247.89</p>
                    </div>
                </li>

                <li id="iphone12pro" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone12pro (1).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 12 Pro</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone12pro')"></button>
                        <p class="fw-bolder fs-4 mb-0">$339.99</p>
                    </div>
                </li>

                <li id="iphone13pro" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone13pro (1).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 13 Pro</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone13pro')"></button>
                        <p class="fw-bolder fs-4 mb-0">$539.99</p>
                    </div>
                </li>


                <li id="iphone14_red" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone14red (1).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 Red</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_red')"></button>
                        <p class="fw-bolder fs-4 mb-0">$539.49</p>
                    </div>
                </li>

                <li id="iphone14_white" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone14white (1).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 White</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_white')"></button>
                        <p class="fw-bolder fs-4 mb-0">$539.49</p>
                    </div>
                </li>

                <li id="iphone14_black" class="list-group-item d-flex justify-content-between">
                    <div class="cart_img" style="min-width: 150px;">
                        <img src="img/for_store/iphone14black (1).png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-lg-grow-1">
                        <div class="px-4">
                            <h4 class="mb-0">Iphone 14 Black</h4>
                            <span class="text-muted">{{__('phones')}}</span>
                        </div>
                    </div>
                    <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_black')"></button>
                        <p class="fw-bolder fs-4 mb-0">$539.49</p>
                    </div>
                </li>

            </ul>


            <div id="emptyMessage" class="alert alert-secondary d-none mt-2 mb-0" role="alert">
                {{__('empty')}}
            </div>

            <div class="d-flex justify-content-end" style="margin-top: 20px;">
                <h4 id="price" >{{__('price')}}</h4>
            </div>

            <div class="d-flex justify-content-between mt-2">
                <a href ="{{ route('store') }}" class="btn btn-outline-dark">
                    {{__('g_store')}}
                </a>
                <button href="#" class="btn btn-outline-dark" data-bs-toggle="modal" onclick="setOrderPrice()" data-bs-target="#personal">
                    {{__("order")}}
                </button>
            </div>
        </div>



        <div id="loader">
            <div class="text-center py-4">
                <div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>

    </div>
</section>

@push('scripts')

    <script defer>
        let price = {
            'buds': 73.99,
            'buds_black': 73.99,
            'buds_white': 73.99,
            'air_pods': 429.99,
            'air_pods_red': 429.99,
            'air_pods_black': 429.99,
            'iphone_gold': 989.99,
            'iphone_black': 989.99,
            'iphone': 989.99,
            'flip_gold': 586.99,
            'flip_lavender': 586.99,
            'flip': 586.99,
            'awatch_red':339.99,
            'awatch_white':339.99,
            'awatch':339.99,
            'gwatch_gray':236.99,
            'gwatch_white':236.99,
            'gwatch':236.99,
            "iphone11pro":247.89,
            "iphone12pro":339.99,
            "iphone13pro":539.99,
            "iphone14_red": 539.49,
            "iphone14_white": 539.49,
            "iphone14_black": 539.49,

        }
        let sum =  0

        var removeCandidate = ''
        window.onload = function () {
            resetCart()
        }

        function resetCart() {
            sum = 0;

            Object.keys(cart).forEach(item => {
                let product = document.getElementById(item)
                let product_mobile = document.getElementById(item + '_mobile')
                if(cart[item]) {
                    if(product) {
                        product.style.display = 'flex'
                    }
                    if(product_mobile) {
                        product_mobile.style.display = 'flex'
                    }
                    if(price[item]){
                        sum += price[item]
                    }
                }
                else{
                    if(product) {
                        product.remove()
                    }
                    if(product_mobile) {
                        product_mobile?.remove()
                    }
                }

                let paragraph = document.getElementById("price");
                let mobile_p = document.getElementById("mobile_price")
                paragraph.textContent = "{{__('price')}} " + String(sum.toFixed(2)) + "$";
                mobile_p.textContent = "{{__('price')}} " + String(sum.toFixed(2)) + "$";

                resetCartNumber()
            })

            setTimeout(function () {
                document.getElementById('loader').classList.add('d-none')
                document.getElementById('mobile_loader').classList.add('d-none')
                document.getElementById('cartContent').classList.remove('d-none')
                document.getElementById('mobile_cartContent').classList.remove('d-none')

                if(Object.values(cart).filter((el)=> el).length === 0) {
                    document.getElementById('emptyMessage').classList.remove('d-none')
                    document.getElementById('mobile_emptyMessage').classList.remove('d-none')
                }
            }, 500)
        }

        function setRemove(product) {
            removeCandidate = product
        }

        function removeProduct() {
            if(!removeCandidate){
                return;
            }
            cart[removeCandidate] = false;
            localStorage.setItem('cart', JSON.stringify(cart))
            resetCart()
        }

        function setOrderPrice() {
            document.getElementById('order_price').innerText = sum.toFixed(2)
        }

    </script>


@endpush
