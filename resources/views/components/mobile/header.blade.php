@props(['page'])


<nav id="mobile_header"  class="navbar navbar-expand-lg navbar-light bg-light d-none fixed-top">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class = "icons_for_header">
            <a style="color: #1E1E1E;" href="{{ route('cart') }}" class="cart_menu {{ $page == 'Cart' ? 'active_page' : null }} position-relative d-flex justify-content-center align-items-center">
                <span id="cart_number_mobile" class="number_badge position-absolute translate-middle badge rounded-pill bg-danger">
                    1
                </span>
                <i class="bi bi-cart2"></i>
            </a>
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    @switch(session('locale'))
                        @case('in')
                            <img src="/img/india.png" height="24">
                            @break
                        @default
                            <img src="/img/uk.png" height="24">
                    @endswitch
                </button>
                <div class="dropdown-menu dropdown-menu-end">

                    <!-- item-->
                    <a href="locale/en" class="dropdown-item notify-item language" data-lang="eng">
                        <img src="/img/uk.png" class="me-1"
                             height="16"> <span class="align-middle">English</span>
                    </a>
                    <!-- item-->
                    <a href="locale/in" class="dropdown-item notify-item language" data-lang="sp">
                        <img src="/img/india.png" class="me-1"
                             height="16"> <span class="align-middle">Hindi</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ $page == 'Home' ? 'active' : '' }}" aria-current="page" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $page == 'Store' ? 'active' : '' }}" href="{{ route('store') }}">Store</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
