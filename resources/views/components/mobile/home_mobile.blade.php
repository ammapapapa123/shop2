<section class="background overall">
    <div class="img_ontop">
        <img src="img/ie_galaxy_buds_live_r180_sm_r180nznaeua_frontmysticbronze_296154303.png" alt="">

    </div>
    <div style="padding: 40% 7% 0;" class="mobile_text">
        <div class="center">
            <h1 class ="text" style="color: #4E4D4D;">
                Headphones <br>
                <span style="color:#AD7067">true</span> samsung
            </h1>
            <h5  class ="text" style="color: #4E4D4D;">
                {{ __('header1.1') }}
                {{ __('header1.2') }}
                {{ __('header1.3') }}
            </h5>
            <div class="center_buttons">
                <a class=" text btn btn-secondary" href="store" role="button">{{__('buy_now')}}</a>
            </div>
        </div>
    </div>

    <div style="padding: 0 7%" class="mobile_text">
        <div class="upper_thing">
            <div>
                <h1 style="color: #494949;">
                    Air <br>
                    Pods<br>
                    Max
                </h1>
            </div>
            <div>
                <img  style="width: 200px" src="img/hero__gnfk5g59t0qe_xlarge.png" class ="one" alt="">
            </div>
        </div>
        <h5 style= "color: #494949">
            {{ __('header2.1') }}
            {{ __('header2.2') }}
            {{ __('header2.3') }}
            {{ __('header2.4') }}
        </h5>
        <div class="center_buttons">
            <a   style=" margin-bottom: 25px" class="btn btn-dark" href="store" role="button">{{__('store')}}</a>
        </div>
    </div>
</section>

<section class="phones_mobile">
        <div class="phones_div">
            <h1 class ="background_text_for_mobile">
                Iphone <br>
                14 <br>
                Pro
            </h1>
            <div style="text-align: center">
                <img style="; width: 300px" src="img/sdsd.png" alt="">
            </div>
            <div class="center_buttons">
                <a  class="btn btn-dark" href="store" role="button">{{__('store')}}</a>
            </div>
        </div>

    <div class="phones_div">
        <h1 class ="background_text_for_mobile">
            Galaxy <br>
            Z Flip <br>
            4 5G
        </h1>
        <div style="text-align: center">
            <img style=" text-align: center; width: 300px" src="img/sdsdsdsd.png" alt="">
        </div>
        <div class="center_buttons">
            <a  class="btn btn-dark" href="store" role="button">{{__('store')}}</a>
        </div>
    </div>

</section>


<section class="phones_mobile">
    <div class="watch_div">
        <h1 class ="background_text_for_mobile">
            Galaxy <br>
            Watch
        </h1>
        <div style="text-align: center">
            <img style="; width: 300px" src="img/samsung.png" alt="">
        </div>
        <div class="center_buttons">
            <a  class="btn btn-dark" href="store" role="button">{{__('store')}}</a>
        </div>
    </div>

    <div class="watch_div">
        <h1 class ="background_text_for_mobile" >
            Apple <br>
            Watch
        </h1>
        <div style="text-align: center">
            <img style=" text-align: center; width: 300px" src="img/apple.png" alt="">
        </div>
        <div class="center_buttons">
            <a  class="btn btn-dark" href="store" role="button">{{__('store')}}</a>
        </div>
    </div>
</section>


<footer>
    <div style="padding: 10% 7% ;" class="mobile_text">
        <div class="center">
            <h1 style="font-weight: 700;font-size: 64px;color: #1E1E1E;">
                Discounts in our store!
            </h1>
            <h5 style="font-weight: 500; font-size: 20px; color: #1E1E1E;">
                {{ __('header3.1') }}
                {{ __('header3.2') }}
                {{ __('header3.3') }}
                {{ __('header3.4') }}
                {{ __('header3.5') }}
            </h5>
            <div class="footer_logos" style="text-align: center">
                <i style="font-size: 15px" class="bi bi-apple"></i>
                <img src="img/black-samsung-logo-png-21.png" alt="">
            </div>
            <div class="center_buttons">
                <a  class="btn btn-dark" href="store" role="button"> {{__('buy_now')}}</a>
            </div>
        </div>
    </div>
</footer>
