<section style="padding: 10% 7%" class="mobile_interface">
    <div class="row_info">
        <div class="c1_info">
            <div class="align-items-center">
                <div>
                    <h3>
                        {{__('partners')}}
                    </h3>
                </div>
                <div class="d-flex">
                    <img style="width: 70px" src="img/icons/1.png" alt="">
                    <img style="width: 70px" src="img/icons/2.png" alt="">
                </div>
            </div>
        </div>

        <div>
            <div>
                <h5>{{__('page_m')}}</h5>
            </div>
            <div>
                <p class="page_m">
                    <a style="text-decoration: none" href="">
                        {{__('home')}}<br>
                    </a>
                    <a style="text-decoration: none" href="store">
                        {{__('store')}}<br>
                    </a>
                </p>
            </div>

        </div>
    </div>
    <div >
        <div>
            <p>
               {{__('header4.4')}}
            </p>
        </div>
        <div>
            {{__('header4.5')}}
        </div>
    </div>
</section>

