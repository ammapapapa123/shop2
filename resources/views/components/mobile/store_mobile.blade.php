<section class="background store_mobile">
    <div class="mobile_outline">
        <div class="filter_part">
            <div class="search">
                <div class="mb-3">
                    <input type="search" class="form-control" id="search_mobile" placeholder="Search">
                </div>
            </div>

            <div class="categories">
                <p>{{__('categories')}}</p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="all"  onclick="categories_mobile('all_mobile')" checked>
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('all')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="headphones" onclick="categories_mobile('headphones_mobile')">
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('headphones')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="phones" onclick="categories_mobile('phones_mobile')">
                    <label class="form-check-label" for="flexRadioDefault2">
                        {{__('phones')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="watch" onclick="categories_mobile('watch_mobile')">
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{__('watch')}}
                    </label>
                </div>
            </div>
            <div class="filter">
                <p >Filter</p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        {{__('discount')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                    <label class="form-check-label" for="flexCheckChecked">
                        {{__('available')}}
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                    <label class="form-check-label" for="flexCheckChecked">
                        {{__('d_available')}}
                    </label>
                </div>
            </div>
            <div style="margin-top: 20px">
                <a href = "cart" type="button" class="btn btn-light"> {{__('cart')}} </a>
            </div>
        </div>

        <div class="mobile_pcards">
            <div class="p_card">
                <div class="card" id="14black_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone14_black">
                            <img src="img/for_store/iphone14black (2).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_black');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_black_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="14red_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone14_red">
                            <img src="img/for_store/iphone14red (3).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_red');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_red_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="14white_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone14_white">
                            <img src="img/for_store/iphone14white (3).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">898.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.49$</h6>
                            </div>
                            <div onclick="toggleClass('iphone14_white');">
                                <a class="btn btn-dark">
                                    <i id ="iphone14_white_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="buds_card_mobile">
                    <div class="img_card slide1">
                        <a href="buds_bronze">
                            <img src="img/for_store/buds_bronze.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live Bronze</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds');">
                                <a class="btn btn-dark">
                                    <i id = "buds_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="buds_black_card_mobile">
                    <div class="img_card slide1">
                        <a href="buds_black">
                            <img src="img/for_store/buds_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live Black</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds_black');">
                                <a class="btn btn-dark">
                                    <i id = "buds_black_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="buds_white_card_mobile">
                    <div class="img_card slide1">
                        <a href="buds_white">
                            <img src="img/for_store/buds_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Samsung Galaxy Buds Live White</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">123.33$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">73.99$</h6>
                            </div>
                            <div onclick="toggleClass('buds_white');">
                                <a class="btn btn-dark">
                                    <i id = "buds_white_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="air_pods_card_mobile">
                    <div class="img_card slide1">
                        <a href="airpods_white">
                            <img src="img/mobile/airpods.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max White</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="air_pods_red_card_mobile">
                    <div class="img_card slide1">
                        <a href="airpods_black">
                            <img src="img/for_store/Air_pods_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max Red</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods_red');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods_red_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="air_pods_black_card_mobile">
                    <div class="img_card slide1">
                        <a href="airpods_red">
                            <img src="img/for_store/Air_pods_red.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Air pods Max Black</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">710.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">425.99$</h6>
                            </div>
                            <div onclick="toggleClass('air_pods_black');">
                                <a class="btn btn-dark">
                                    <i id = "air_pods_black_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card" id="iphone_gold_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone_gold">
                            <img src="img/for_store/iphone_gold.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max Gold</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div>
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone_gold');">
                                <a class="btn btn-dark">
                                    <i id="iphone_gold_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="iphone_black_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone_black">
                            <img src="img/for_store/iphone_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max Black</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone_black');">
                                <a class="btn btn-dark">
                                    <i id="iphone_black_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="iphone_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone_white">
                            <img src="img/mobile/iphone.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 14 Pro Max White</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">1649.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">989.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone');">
                                <a class="btn btn-dark">
                                    <i id="iphone_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_card_mobile">
                    <div class="img_card slide1">
                        <a href="flip_black">
                            <img src="img/for_store/flip_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Gray</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip');">
                                <a class="btn btn-dark">
                                    <i id="flip_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_gold_card_mobile">
                    <div class="img_card slide1">
                        <a href="flip_gold">
                            <img src="img/for_store/flip_ gold.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Gold</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip_gold');">
                                <a class="btn btn-dark">
                                    <i id="flip_gold_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="flip_lavender_card_mobile">
                    <div class="img_card slide1">
                        <a href="flip_lavender">
                            <img src="img/for_store/flip_lavender.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">GalaxyZ Flip4 5G Lavender</h6>
                        </div>
                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">978.33$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">586.99$</h6>
                            </div>
                            <div onclick="toggleClass('flip_lavender');">
                                <a class="btn btn-dark">
                                    <i id="flip_lavender_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_card_mobile">
                    <div class="img_card slide1">
                        <a href="awatch_black">
                            <img src="img/for_store/apple_watch_black.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple  Watch Black</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch');">
                                <a class="btn btn-dark">
                                    <i id ="awatch_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_red_card_mobile">
                    <div class="img_card slide1">
                        <a href="awatch_red">
                            <img src="img/for_store/apple_watch_red.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple Watch Red </h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch_red');">
                                <a class="btn btn-dark">
                                    <i id ="awatch_red_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="awatch_white_card_mobile">
                    <div class="img_card slide1">
                        <a href="awatch_white">
                            <img src="img/for_store/apple_watch_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Apple Watch Beige</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">566.66$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">339.99$</h6>
                            </div>
                            <div onclick="toggleClass('awatch_white');">
                                <a class="btn btn-dark">
                                    <i id ="awatch_white_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="gwatch_card_mobile">
                    <div class="img_card slide1">
                        <a href="gwatch_blue">
                            <img src="img/for_store/galaxy_watch_blue.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch Blue</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="gwatch_gray_card_mobile">
                    <div class="img_card slide1">
                        <a href="gwatch_gray">
                            <img src="img/for_store/galaxy_watch_gray.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch Gray</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch_gray');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch_gray_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="gwatch_white_card_mobile">
                    <div class="img_card slide1">
                        <a href="gwatch_white">
                            <img src="img/for_store/galaxy_watch_white.png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem;">
                        <div style="text-align: center">
                            <h6 class="card-title">Galaxy Watch White</h6>
                        </div>

                        <div class="card-body d-flex" style="align-items: center; border-top: solid 2px;">
                            <div>
                                <h6 class="card-title"><span style = "text-decoration: line-through;">394.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">236.99$</h6>
                            </div>
                            <div onclick="toggleClass('gwatch_white');">
                                <a class="btn btn-dark">
                                    <i id = "gwatch_white_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="11pro_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone11pro">
                            <img src="img/for_store/iphone11pro(4).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 11 Pro</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">412.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">247.89$</h6>
                            </div>
                            <div onclick="toggleClass('iphone11pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone11pro_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="12pro_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone12pro">
                            <img src="img/for_store/iphone12pro (1).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 12 Pro</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">599.00$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">369.00$</h6>
                            </div>
                            <div onclick="toggleClass('iphone12pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone12pro_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="13pro_card_mobile">
                    <div class="img_card slide1">
                        <a href="iphone13pro">
                            <img src="img/for_store/iphone13pro (2).png" class="card-img-top" alt="...">
                        </a>
                    </div>
                    <div style="padding: 0 1rem">
                        <div style="text-align: center">
                            <h6 class="card-title">Iphone 13 Pro</h6>
                        </div>
                        <div class="card-body d-flex" style="; align-items: center; border-top: solid 2px;">
                            <div >
                                <h6 class="card-title"><span style = "text-decoration: line-through;">899.99$</span></h6>
                            </div>
                            <div >
                                <h6 class="card-title">539.99$</h6>
                            </div>
                            <div onclick="toggleClass('iphone13pro');">
                                <a class="btn btn-dark">
                                    <i id ="iphone13pro_mobile" class="bi bi-heart"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

@push('scripts')
<script defer>

    let products_mobile = [
        {
            "id": "buds_card_mobile",
            "name": "Samsung Galaxy Buds Live",
            "type": "headphones_mobile",
            "price": 73.99,
        },
        {
            "id": "buds_black_card_mobile",
            "name": "Samsung Galaxy Buds Live",
            "type": "headphones_mobile",
            "price": 73.99,
        },
        {
            "id": "buds_white_card_mobile",
            "name": "Samsung Galaxy Buds Live",
            "type": "headphones_mobile",
            "price": 73.99,
        },
        {
            "id": "air_pods_card_mobile",
            "name": "Air pods Max",
            "type": "headphones_mobile",
            "price": 425.99,
        },
        {
            "id": "air_pods_black_card_mobile",
            "name": "Air pods Max",
            "type": "headphones_mobile",
            "price": 425.99,
        },
        {
            "id": "air_pods_red_card_mobile",
            "name": "Air pods Max",
            "type": "headphones_mobile",
            "price": 425.99,
        },
        {
            "id": "air_pods_card_mobile",
            "name": "Air pods Max",
            "type": "headphones_mobile",
            "price": 425.99,
        },

        {
            "id": "iphone_card_mobile",
            "name": "Iphone 14 Pro Max",
            "type": "phones_mobile",
            "price": 989.99,
        },
        {
            "id": "iphone_black_card_mobile",
            "name": "Iphone 14 Pro Max",
            "type": "phones_mobile",
            "price": 989.99,
        },
        {
            "id": "iphone_gold_card_mobile",
            "name": "Iphone 14 Pro Max",
            "type": "phones_mobile",
            "price": 989.99,
        },
        {
            "id": "flip_card_mobile",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones_mobile",
            "price": 586.99,
        },
        {
            "id": "flip_gold_card_mobile",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones_mobile",
            "price": 586.99,
        },
        {
            "id": "flip_lavender_card_mobile",
            "name": "GalaxyZ Flip4 5G",
            "type": "phones_mobile",
            "price": 586.99,
        },
        {
            "id": "awatch_card_mobile",
            "name": "Apple Watch",
            "type": "watch_mobile",
            "price": 339.99,

        },
        {
            "id": "awatch_red_card_mobile",
            "name": "Apple Watch",
            "type": "watch_mobile",
            "price": 339.99,

        },
        {
            "id": "awatch_white_card_mobile",
            "name": "Apple Watch",
            "type": "watch_mobile",
            "price": 339.99,

        },
        {
            "id": "gwatch_card_mobile",
            "name": "Galaxy Watch",
            "type": "watch_mobile",
            "price": 236.99,
        },
        {
            "id": "gwatch_gray_card_mobile",
            "name": "Galaxy Watch",
            "type": "watch_mobile",
            "price": 236.99,
        },
        {
            "id": "gwatch_white_card_mobile",
            "name": "Galaxy Watch",
            "type": "watch_mobile",
            "price": 236.99,
        },

        {
            "id": "11pro_card_mobile",
            "name": "Iphone 11 Pro",
            "type": "phones_mobile",
            "price": 236.99,
        },

        {
            "id": "12pro_card_mobile",
            "name": "Iphone 12 Pro",
            "type": "phones_mobile",
            "price": 236.99,
        },
        {
            "id": "13pro_card_mobile",
            "name": "Iphone 13 Pro",
            "type": "phones_mobile",
            "price": 236.99,
        },

        {
            "id": "14black_card_mobile",
            "name": "Iphone 14 Black",
            "type": "phones_mobile",
            "price": 236.99,
        },

        {
            "id": "14white_card_mobile",
            "name": "Iphone 14 White",
            "type": "phones_mobile",
            "price": 236.99,
        },

        {
            "id": "14red_card_mobile",
            "name": "Iphone 14 Red",
            "type": "phones_mobile",
            "price": 236.99,
        },
    ]


    const mobile_search_input = document.getElementById("search_mobile")

    mobile_search_input.addEventListener("input", e => {
        const value = e.target.value.toLowerCase()
        products_mobile.forEach(product => {
            const can_be_seen =  product.name.toLowerCase().includes(value)
            let element = document.getElementById(product.id)
            if(can_be_seen){
                console.log(product.id)
                element.classList.add('dis')
                element.classList.remove('hide')
            }else{
                element.classList.add("hide")
                element.classList.remove('dis')
            }
        })
    })
    function categories_mobile(key){
        products_mobile.forEach(ct => {
            let element = document.getElementById(ct.id)

            if(key === 'all_mobile'){
                element.classList.remove('hide')
            }else{
                if(ct.type !== key){
                    element.classList.add('hide')
                    element.classList.remove('dis')
                }else{
                    element.classList.add('dis')
                    element.classList.remove('hide')
                }
            }
        })
    }
</script>
@endpush

