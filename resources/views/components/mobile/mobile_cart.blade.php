<section class="mobile_cart_section pt-5">
   <div class="mobile_cart">
       <div class="my-2">
           <h4>{{__('shop_cart')}}</h4>
       </div>
       <div id="mobile_cartContent" class="d-none">
           <ul class="list-group">

               <li id="buds_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/buds_bronze.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Samsung Galaxy Buds Live Bronze</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds')"></button>
                       <p class="fw-bolder fs-4 mb-0">$73.99</p>
                   </div>
               </li>

               <li id="buds_black_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/buds_black.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Samsung Galaxy Buds Live Black</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds')"></button>
                       <p class="fw-bolder fs-4 mb-0">$73.99</p>
                   </div>
               </li>

               <li id="buds_white_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/buds_white.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Samsung Galaxy Buds Live White</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('buds')"></button>
                       <p class="fw-bolder fs-4 mb-0">$73.99</p>
                   </div>
               </li>

               <li id="air_pods_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/mobile/airpods.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Air Pods Max White</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods')"></button>
                       <p class="fw-bolder fs-4 mb-0">$429.99</p>
                   </div>
               </li>

               <li id="air_pods_red_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/Air_pods_red.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Air Pods Max Red</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods')"></button>
                       <p class="fw-bolder fs-4 mb-0">$429.99</p>
                   </div>
               </li>

               <li id="air_pods_black_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/Air_pods_black.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Air Pods Max Black</h4>
                           <span class="text-muted">{{__('headphones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('air_pods')"></button>
                       <p class="fw-bolder fs-4 mb-0">$429.99</p>
                   </div>
               </li>


               <li id="iphone_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/mobile/iphone.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Iphone 14 Pro Max</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone')"></button>
                       <p class="fw-bolder fs-4 mb-0">$989.99</p>
                   </div>
               </li>

               <li id="iphone_gold_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/iphone_gold.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Iphone 14 Pro Max Gold</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone')"></button>
                       <p class="fw-bolder fs-4 mb-0">$989.99</p>
                   </div>
               </li>

               <li id="iphone_black_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/iphone_black.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Iphone 14 Pro Max Black</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone')"></button>
                       <p class="fw-bolder fs-4 mb-0">$989.99</p>
                   </div>
               </li>

               <li id="flip_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/flip_black.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">GalaxyZ Flip4 5G Black</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip')"></button>
                       <p class="fw-bolder fs-4 mb-0">$586.99</p>
                   </div>
               </li>

               <li id="flip_gold_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/flip_ gold.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">GalaxyZ Flip4 5G Gold</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip')"></button>
                       <p class="fw-bolder fs-4 mb-0">$586.99</p>
                   </div>
               </li>

               <li id="flip_lavender_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/flip_lavender.pngg" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">GalaxyZ Flip4 5G Lavender</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('flip')"></button>
                       <p class="fw-bolder fs-4 mb-0">$586.99</p>
                   </div>
               </li>

               <li id="awatch_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/apple_watch_black.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Apple watch 8 series Black</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$339.99</p>
                   </div>
               </li>

               <li id="awatch_red_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/apple_watch_red.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Apple watch 8 series Red</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$339.99</p>
                   </div>
               </li>

               <li id="awatch_white_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/apple_watch_white.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Apple watch 8 series White</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('awatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$339.99</p>
                   </div>
               </li>


               <li id="gwatch_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/galaxy_watch_blue.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Galaxy Watch Blue</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$236.99</p>
                   </div>
               </li>
               <li id="gwatch_gray_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/galaxy_watch_gray.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Galaxy Watch Gray</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$236.99</p>
                   </div>
               </li>

               <li id="gwatch_white_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img">
                       <img src="img/for_store/galaxy_watch_white.png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-grow-1">
                       <div style="padding-left: 10px;">
                           <h4 class="mb-0 fs-6">Galaxy Watch White</h4>
                           <span class="text-muted">{{__('watch')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('gwatch')"></button>
                       <p class="fw-bolder fs-4 mb-0">$236.99</p>
                   </div>
               </li>

               <li id="iphone11pro_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone11pro(3).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 11 Pro</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone11pro_mobile')"></button>
                       <p class="fw-bolder fs-4 mb-0">$247.89</p>
                   </div>
               </li>

               <li id="iphone12pro_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone12pro (1).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 12 Pro</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone12pro')"></button>
                       <p class="fw-bolder fs-4 mb-0">$339.99</p>
                   </div>
               </li>

               <li id="iphone13pro_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone13pro (1).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 13 Pro</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone13pro_mobile')"></button>
                       <p class="fw-bolder fs-4 mb-0">$539.99</p>
                   </div>
               </li>


               <li id="iphone14_red_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone14red (1).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 14 Red</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_red_mobile')"></button>
                       <p class="fw-bolder fs-4 mb-0">$539.49</p>
                   </div>
               </li>

               <li id="iphone14_white_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone14white (1).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 14 White</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_white_mobile')"></button>
                       <p class="fw-bolder fs-4 mb-0">$539.49</p>
                   </div>
               </li>

               <li id="iphone14_black_mobile" class="list-group-item d-flex justify-content-between">
                   <div class="cart_img" style="min-width: 150px;">
                       <img src="img/for_store/iphone14black (1).png" alt="">
                   </div>
                   <div class="d-flex align-items-center flex-lg-grow-1">
                       <div class="px-4">
                           <h4 class="mb-0">Iphone 14 Black</h4>
                           <span class="text-muted">{{__('phones')}}</span>
                       </div>
                   </div>
                   <div style="text-align: right; align-items: end;" class="d-flex flex-column justify-content-between">
                       <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="setRemove('iphone14_black_mobile')"></button>
                       <p class="fw-bolder fs-4 mb-0">$539.49</p>
                   </div>
               </li>

           </ul>

           <div id="mobile_emptyMessage" class="alert alert-secondary d-none mt-2 mb-0" role="alert">
               {{__('empty')}}
           </div>

           <div class="d-flex justify-content-end mt-3">
               <h4 id="mobile_price" >Total Price:</h4>
           </div>

           <div class="d-flex justify-content-between" style="margin-top: 30px">
               <a href ="store" class="btn btn-outline-dark">
                   {{__('g_store')}}
               </a>
               <button href="#" class="btn btn-outline-dark" onclick="setOrderPrice()" data-bs-toggle="modal" data-bs-target="#personal">
                   {{__('order')}}
               </button>
           </div>
       </div>

       <div id="mobile_loader">
           <div class="text-center py-4">
               <div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                   <span class="visually-hidden">Loading...</span>
               </div>
           </div>
       </div>

   </div>
</section>
