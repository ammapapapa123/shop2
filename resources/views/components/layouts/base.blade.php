@props(['page'])
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/header.css">

    <!-- head stack -->
    @stack('head')

    <!-- bootstrap -->
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <script src="/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">


    <title>{{ $page ?? 'Market' }}</title>
</head>
<body>
    <!-- desktop header -->
    <x-desktop.header :page="$page ?? null"></x-desktop.header>
    <!-- mobile header -->
    <x-mobile.header :page="$page ?? null"></x-mobile.header>


    {{ $slot }}


    <!-- desktop footer -->
    <x-desktop.end_info :page="$page ?? null"></x-desktop.end_info>
    <!-- mobile footer -->
    <x-mobile.mobile_end_info :page="$page ?? null"></x-mobile.mobile_end_info>

@stack('scripts')
</body>
</html>
