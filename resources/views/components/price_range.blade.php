
        <div class="price-input">
            <div class="field">
                <span>{{__('min')}}</span>
                <input type="number" class="input-min" value="0">
            </div>
            <div class="separator">-</div>
            <div class="field">
                <span>{{__('max')}}</span>
                <input type="number" class="input-max" value="1500">
            </div>
        </div>
        <div class="slider">
            <div class="progress"></div>
        </div>
        <div class="range-input">
            <input type="range" id ="range1" class="range-min" min="0" max="1500" value="0" step="100">
            <input type="range" id="range2" class="range-max" min="0" max="1500" value="1500" step="100">
        </div>





<script defer>
    const range1_input = document.getElementById('range1')
    const range2_input = document.getElementById('range2')

    range1_input.addEventListener("input", range_number => {
        const value = range_number.target.value.toLowerCase()
        console.log(value)
        products.forEach(product => {
            const something = parseInt(value) < product.price
            console.log(something)
            console.log(product.name)
            let element = document.getElementById(product.id)
            element.classList.toggle("hide", !something)
        })
    })

    range2_input.addEventListener("input", range_number => {
        const value = range_number.target.value.toLowerCase()
        console.log(value)
        products.forEach(product => {
            const something = parseInt(value) > product.price;
            console.log(something)
            console.log(product.name)
            let element = document.getElementById(product.id)
            element.classList.toggle("hide", !something)
        })
    })



    const rangeInput = document.querySelectorAll(".range-input input"),
        priceInput = document.querySelectorAll(".price-input input"),
        range = document.querySelector(".slider .progress");
    let priceGap = 200;

    priceInput.forEach((input) => {
        input.addEventListener("input", (e) => {
            let minPrice = parseInt(priceInput[0].value),
                maxPrice = parseInt(priceInput[1].value);

            if (maxPrice - minPrice >= priceGap && maxPrice <= rangeInput[1].max) {
                if (e.target.className === "input-min") {
                    rangeInput[0].value = minPrice;
                    range.style.left = (minPrice / rangeInput[0].max) * 100 + "%";
                } else {
                    rangeInput[1].value = maxPrice;
                    range.style.right = 100 - (maxPrice / rangeInput[1].max) * 100 + "%";
                }
            }
        });
    });

    rangeInput.forEach((input) => {
        input.addEventListener("input", (e) => {
            let minVal = parseInt(rangeInput[0].value),
                maxVal = parseInt(rangeInput[1].value);

            if (maxVal - minVal < priceGap) {
                if (e.target.className === "range-min") {
                    rangeInput[0].value = maxVal - priceGap;
                } else {
                    rangeInput[1].value = minVal + priceGap;
                }
            } else {
                priceInput[0].value = minVal;
                priceInput[1].value = maxVal;
                range.style.left = (minVal / rangeInput[0].max) * 100 + "%";
                range.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
            }
        });
    });

</script>
