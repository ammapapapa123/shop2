<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <x-desktop.cart_things></x-desktop.cart_things>
        </section>
    </section>
    </section>
    <section class="mobile_interface" style="min-height: 100vh;">
        <section class="background">
            <x-mobile.mobile_cart   ></x-mobile.mobile_cart>
        </section>
    </section>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="py-1 modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal" onclick="removeProduct()">Remove</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="personal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="card-header">
                    <h3 class="mb-0">Order information</h3>
                </div>
                <div class="modal-body p-3">
                    <form class="input_section row g-3">
                        <div class="col-md-12">
                            <label for="input_full_name" class="form-label">Full name</label>
                            <input type="text" class="form-control" id="inputCard_number"  placeholder="John Smith">
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="inputEmail" class="form-label">Email</label>
                            <input type="text" class="form-control" id="inputZip"placeholder="someone@example.com">
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                        <div class="col-md-8">
                            <label for="input_address" class="form-label">Address</label>
                            <input type="text" class="form-control" id="inputCVV" placeholder="123 A. 1st Street">
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip" class="form-label" >Zip</label>
                            <input type="text" class="form-control" id="inputName" placeholder="00000">
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>

                    </form>
                    <div class="mt-3 d-flex justify-content-between">
                        <span class="">Order Price</span>
                        <span><span id="order_price">300</span>$</span>
                    </div>
                </div>
                <hr>
                <div class="pb-3 px-3 d-flex justify-content-between">
                    <button href="cart" data-bs-dismiss="modal" class="btn btn-outline-dark"> Cancel</button>
                    <a href="{{ route('payment') }}" class="btn btn-outline-dark"> Proceed to payment</a>
                </div>
            </div>
        </div>
    </div>





    @push('head')
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/cart.css">
        <link rel="stylesheet" href="css/mobile/cart_mobile.css">
    @endpush
</x-layouts.base>
