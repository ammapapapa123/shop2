<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>Apple Watch Series 7 GPS Beige</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/apple_watch_white.png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/apple_watch_white.png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/awatch_white1 (1).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/awatch_white1 (2).png" onclick = replace('3') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "4" src="img/for_store/awatch_white1 (3).png" onclick = replace('4') alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <div >
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="awatch_red"  style="  background-color: #DB3044; border: 1px solid #DB3044;"    class="colour_button button1">
                                        </a>
                                        <a href="awatch_white" style =" box-shadow: 0 0 10px gray; border: 1px solid #F5F5DC; background-color: #F5F5DC;" class="colour_button button2">

                                        </a>
                                        <a  href="awatch_black" style = " border: 1px solid #696969; background-color: #696969;" class="colour_button button3">
                                        </a>
                                    </div>
                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Series</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Apple Watch Series 7</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Operating system</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Watch OS</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>The shape of the watch case</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Rectangle</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Case Material</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Aluminium</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Update frequencyn</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>120 Hz</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Bracelet/strap</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Fluoro rubber</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Internal memory capacity</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>32</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Wireless charging</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Yes</p>
                                            </div>
                                        </div>


                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Moisture protection</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>WR50</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Contactless payment</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Yes</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Weight, gr</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>32</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>$339.99</h5>
                        </div>
                        <div>
                           {{__('date_pickup')}}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From $339.99 or $14.16/mo. for 24 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('apple_watch_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>Apple Watch Series 7 GPS Beige</h5>
                </div>

                <div class="the_middle">
                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/apple_watch_white.png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/apple_watch_white.png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/awatch_white1 (1).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/awatch_white1 (2).png" onclick = replace('7') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="8" src="img/for_store/awatch_white1 (3).png" onclick = replace('8') alt="">
                            </div>
                        </div>
                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div >
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="awatch_red"  style=" background-color: #DB3044;  border: 1px solid #DB3044;"    class="colour_button button1">
                            </a>
                            <a href="awatch_white" style =" box-shadow: 0 0 10px gray; border: 1px solid #F5F5DC; background-color: #F5F5DC;" class="colour_button button2">

                            </a>
                            <a  href="awatch_black" style = " border: 1px solid #696969; background-color: #696969;" class="colour_button button3">
                            </a>
                        </div>
                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Series</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Apple Watch Series 7</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Operating system</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Watch OS</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>The shape of the watch case</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Rectangle</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Case Material</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Aluminium</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Update frequencyn</p>
                                </div>
                                <div style="text-align: right">
                                    <p>120 Hz</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Bracelet/strap</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Fluoro rubber</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Internal memory capacity</p>
                                </div>
                                <div style="text-align: right">
                                    <p>32</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Wireless charging</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Yes</p>
                                </div>
                            </div>


                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Moisture protection</p>
                                </div>
                                <div style="text-align: right">
                                    <p>WR50</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Contactless payment</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Yes</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Weight, gr</p>
                                </div>
                                <div style="text-align: right">
                                    <p>32</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>$339.99</h5>
                </div>
                <div>
                   {{__('date_pickup')}}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From $339.99 or $14.16/mo. for 24 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

