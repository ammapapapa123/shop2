<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>GalaxyZ Flip4 5G Gray 256GB</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/flip_black.png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/flip_black.png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/flip_gray1 (1).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/flip_gray1 (2).png" onclick = replace('3') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "4" src="img/for_store/flip_gray1 (3).png" onclick = replace('4') alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <div >
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="flip_lavender"  style=" background-color: #E6E6FA; color: black; border: 1px solid #E6E6FA;"    class="colour_button button1">
                                        </a>
                                        <a href="flip_gold" style ="border: 1px solid #e0c56e; background-color: #e0c56e;" class="colour_button button2">

                                        </a>
                                        <a  href="flip_black" style = "box-shadow: 0 0 10px gray; border: 1px solid gray; background-color: gray;" class="colour_button button3">
                                        </a>
                                    </div>
                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>HD Model Year</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>2022</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Display diagonal, inch</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>6.7</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Display resolution</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>1080x2640</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Matrix Type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>AMOLED</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Update frequencyn</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>120 Hz</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>The amount of RAM, GB</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>8</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Processor Model</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Snapdragon 8+ Gen 1</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Operating system</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Android 12</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Display resolution</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>1179x2556</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Interfaces</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Bluetooth, NFC, Wi-Fi</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Dust and moisture protection standard</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>IPX8</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>$586.99</h5>
                        </div>
                        <div>
                       {{__('date_pickup')}}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">G{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From $586.99 or $24.45/mo. for 24 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('flip_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>GalaxyZ Flip4 5G Gray 256GB</h5>
                </div>

                <div class="the_middle">
                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/flip_black.png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/flip_black.png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/flip_gray1 (1).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/flip_gray1 (2).png" onclick = replace('7') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="8" src="img/for_store/flip_gray1 (3).png" onclick = replace('8') alt="">
                            </div>
                        </div>
                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div >
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="flip_lavender"  style=" background-color: #E6E6FA; color: black; border: 1px solid #E6E6FA;"    class="colour_button button1">
                            </a>
                            <a href="flip_gold" style ="border: 1px solid #e0c56e; background-color: #e0c56e;" class="colour_button button2">

                            </a>
                            <a  href="flip_black" style = "box-shadow: 0 0 10px gray; border: 1px solid gray; background-color: gray;" class="colour_button button3">
                            </a>
                        </div>
                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>HD Model Year</p>
                                </div>
                                <div style="text-align: right">
                                    <p>2022</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display diagonal, inch</p>
                                </div>
                                <div style="text-align: right">
                                    <p>6.7</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display resolution</p>
                                </div>
                                <div style="text-align: right">
                                    <p>1080x2640</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Matrix Type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>AMOLED</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Update frequencyn</p>
                                </div>
                                <div style="text-align: right">
                                    <p>120 Hz</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>The amount of RAM, GB</p>
                                </div>
                                <div style="text-align: right">
                                    <p>8</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Processor Model</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Snapdragon 8+ Gen 1</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Operating system</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Android 12</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display resolution</p>
                                </div>
                                <div style="text-align: right">
                                    <p>1179x2556</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Interfaces</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Bluetooth, NFC, Wi-Fi</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Dust and moisture protection standard</p>
                                </div>
                                <div style="text-align: right">
                                    <p>IPX8</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>$586.99</h5>
                </div>
                <div>
               {{__('date_pickup')}}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">G{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From $586.99 or $24.45/mo. for 24 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

