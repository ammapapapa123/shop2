<x-layouts.base :page="$page">
    <section class="desktop_interface" style="min-height: 100vh;">
        <section class="background">
            <sectoin>
                <div class="upper_half d-flex">
                    <div class="the_product_part">
                        <div class="title" style="margin-bottom: 20px">
                            <h5>Apple iPhone 14 White</h5>
                        </div>
                        <div class="the_middle">
                            <div class="images">
                                <div class="upper_image">
                                    <img id = "i" src="img/for_store/iphone14white (1).png" alt="">
                                </div>
                                <div class="bottom_img">
                                    <div class="frames">
                                        <img id= "1"src="img/for_store/iphone14white (1).png" onclick = replace('1') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "2" src="img/for_store/iphone14white (2).png" onclick = replace('2') alt="">
                                    </div>
                                    <div class="frames">
                                        <img id= "3"src="img/for_store/iphone14white (3).png" onclick = replace('3') alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="description">
                                <div >
                                    <p style="font-weight: bold">Colour</p>
                                    <div>
                                        <a href="iphone14_white"  style="box-shadow: 0 0 10px gray; background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                                        </a>
                                        <a href="iphone14_red" style =" box-shadow: 0 0 10px gray; border: 1px solid #DB3044; background-color: #DB3044;" class="colour_button button2">

                                        </a>
                                        <a  href="iphone14_black" style = "border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                                        </a>
                                    </div>
                                </div>

                                <div class="d_text">
                                    <p style="font-weight: bold">
                                        Description
                                    </p>
                                    <div class="d_text_info">
                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Model year</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>2022</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Screen</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>6.1"/2532x1170 Pixels</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Screen Technology</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>OLED</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Processor type</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>A14 Bionic</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Built-in memory (ROM)</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>128 GB</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Main Camera Megapixel</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>12/12/12</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Video resolution</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>3840x2160 Pixels (4K)</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Optical stabilization </p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>Yes</p>
                                            </div>
                                        </div>

                                        <div class="line_d_text">
                                            <div class="one_of_d_text">
                                                <p>Optical zoom for magnification</p>
                                            </div>
                                            <div style="text-align: right">
                                                <p>x2</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="the_price_part">
                        <div>
                            <h5>$539.49</h5>
                        </div>
                        <div>
                            {{ __('date_pickup') }}
                        </div>
                        <div style="margin: 30px 0 ">
                            <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                        </div>

                        <div>
                            <h5>From $539.49 or  $22.49/mo for 24 mo.</h5>
                        </div>

                        <div>
                            <p>
                                {{ __('iphone14_description') }}
                            </p>
                        </div>
                    </div>
                </div>
            </sectoin>

        </section>
    </section>

    <section class="mobile_interface">
        <section class="background" style="padding: 7% 3%;">
            <div class="the_product_part_mobile" style=" margin-top: 50px;">
                <div class="title" style="margin-bottom: 20px">
                    <h5>Apple iPhone 14 White</h5>
                </div>

                <div class="the_middle">
                    <div class="images d-flex" style="gap: 4rem">
                        <div class="upper_image_mobile">
                            <img  src="img/for_store/iphone14white (1).png" id="mi"    alt="">
                        </div>
                        <div class="bottom_img_mobile">
                            <div class="frames_mobile">
                                <img  id="5" src="img/for_store/iphone14white (1).png" onclick = replace('5') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="6" src="img/for_store/iphone14white (2).png"onclick = replace('6') alt="">
                            </div>
                            <div class="frames_mobile">
                                <img id="7" src="img/for_store/iphone14white (3).png" onclick = replace('7') alt="">
                            </div>
                        </div>

                        <x-pop_up></x-pop_up>
                    </div>
                </div>

                <div>
                    <div >
                        <p style="font-weight: bold">Colour</p>
                        <div>
                            <a href="iphone14_white"  style=" background-color: white; color: black; border: 1px solid black;"    class="colour_button button1">
                            </a>
                            <a href="iphone14_red" style =" box-shadow: 0 0 10px gray; border: 1px solid #DB3044; background-color: #DB3044;" class="colour_button button2">

                            </a>
                            <a  href="iphone14_black" style = "border: 1px solid black; color: white; background-color: black;" class="colour_button button3">
                            </a>
                        </div>
                    </div>

                    <div class="d_text">
                        <p style="font-weight: bold">
                            Description
                        </p>
                        <div class="d_text_info">
                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Model year</p>
                                </div>
                                <div style="text-align: right">
                                    <p>2022</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display diagonal, inch</p>
                                </div>
                                <div style="text-align: right">
                                    <p>6.1</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display resolution</p>
                                </div>
                                <div style="text-align: right">
                                    <p>2532x1170</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Matrix Type</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Super Retina XBM</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Update frequency</p>
                                </div>
                                <div style="text-align: right">
                                    <p>60 Hz</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>The amount of RAM, GB</p>
                                </div>
                                <div style="text-align: right">
                                    <p>6</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Internal memory capacity, GB</p>
                                </div>
                                <div style="text-align: right">
                                    <p>128</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Processor Model</p>
                                </div>
                                <div style="text-align: right">
                                    <p>A16 Bionic</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Display resolution</p>
                                </div>
                                <div style="text-align: right">
                                    <p>1179x2556</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Operating system</p>
                                </div>
                                <div style="text-align: right">
                                    <p>iOS 16</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Interfaces</p>
                                </div>
                                <div style="text-align: right">
                                    <p>Bluetooth, NFC, Wi-Fi</p>
                                </div>
                            </div>


                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Dust and moisture protection standard</p>
                                </div>
                                <div style="text-align: right">
                                    <p>IP68</p>
                                </div>
                            </div>

                            <div class="line_d_text">
                                <div class="one_of_d_text">
                                    <p>Weight, gr</p>
                                </div>
                                <div style="text-align: right">
                                    <p>176</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            </div>
            <div class="the_price_part_mobile"`>
                <div>
                    <h5>$539.49</h5>
                </div>
                <div>
                    {{ __('date_pickup') }}
                </div>
                <div style="margin: 30px 0 ">
                    <a  class="btn btn-dark" href="store" role="button">{{__('g_store')}}</a>
                </div>

                <div>
                    <h5>From $539.49 or  $22.49/mo for 24 mo.</h5>
                </div>
            </div>
        </section>
    </section>
    @push('head')
        <link rel="stylesheet" href="css/product_page.css">
        <link rel="stylesheet" href="css/end_info.css">
        <link rel="stylesheet" href="css/header.css">
    @endpush
</x-layouts.base>

