<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ $page }}</title>

    <!-- bootstrap -->
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <script src="/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">

    <link rel="stylesheet" href="css/loader.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div style="min-height: 100vh;" class="d-flex justify-content-center flex-column">
        <div id="loader">
            <div class="loader"></div>
        </div>
        <div class="card mx-auto w-50 p-4 d-none pb-3 shadow-lg" id="payment_box">
            <div>
                <h2>Payment Info</h2>
            </div>
            <hr>
            <form class="input_section row g-3" id="payment_form">
                @csrf
                <div class="col-md-8">
                    <label for="card_number" class="form-label">Card number</label>
                    <div class="input-group">
                        <div class="input-group-text"><i id = "TargetObject" class="bi bi-credit-card"></i></div>
                        <input type="text" class="form-control" name="card_number" id="card_number" data-format="****-****-****-****" data-mask="xxxx-xxxx-xxxx-xxxx">
                    </div>
                    <div class="invalid-feedback" id="card_number_error">
                        Incorrect Card number
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="cvv" class="form-label">CVV/CVC code</label>
                    <input type="number" class="form-control" name="cvv" id="cvv" placeholder="000">
                    <div class="invalid-feedback" id="cvv_error">
                        Incorrect CVV
                    </div>
                </div>
                <div class="col-md-8"   >
                    <label for="name" class="form-label" >Name on card</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="John Smith">
                    <div class="invalid-feedback" id="name_error">
                        Incorrect Name
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="exp_date" class="form-label">Expiry</label>
                    <input type="text" class="form-control" name="exp_date" id="exp_date" data-format="**/**" data-mask="MM/YY">
                    <div class="invalid-feedback" id="exp_date_error">
                        Incorrect Expiration Date
                    </div>
                </div>
                <hr>
                <div class="last_add mt-0 l-12 d-flex justify-content-end">
                    <button type="button" onclick="formSubmit()" class="last_buttons btn btn-dark">Proceed</button>
                </div>
            </form>
        </div>
    </div>


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary d-none" id="final_modal" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Launch demo modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert">
                        Internal Server Error
                    </div>
                    <p class="text-muted">
                        Please try later
                    </p>
                    <p class="text-muted mb-0">
                        You will be redirected back in 5 seconds...
                    </p>
                </div>
            </div>
        </div>
    </div>


    <script defer>

        const car_input = document.getElementById("card_number")

        car_input.addEventListener("input", e => {
            const value = e.target.value.toLowerCase()
            let ncard = GetCardType(value)

            let Obj = document.getElementById('TargetObject');
            if(ncard === "Mastercard"){
                let str = '<img style="width: 16px" id = "TargetObject" src="img/icons/mastercard.png" alt="">';
                Obj.outerHTML=str;
            }if(ncard === "Visa"){
                let str = '<img style="width: 16px" id = "TargetObject" src="img/icons/visa.png" alt="">';
                Obj.outerHTML=str;
            }else{
                let str = '<i id = "TargetObject" class="bi bi-credit-card">';
                Obj.outerHTML=str;
            }

        })

        function GetCardType(number)
        {
            var re = new RegExp("^4");
            if (number.match(re) != null)
                return "Visa";

            var re = new RegExp("^5");
            if (number.match(re) != null)
                return "Mastercard";

            return "no";
        }

        async function formSubmit(){
            clearInvalid()

            let form = document.querySelector('#payment_form');
            let elements = Array.from(form.elements);

            let body = {}

            elements.forEach(item => {
                body[item.id] = item.value
            })

            fetch('{{ route('make-payment') }}', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                body: JSON.stringify(body)
            })
                .then(response => response.json())
                .then(response => {
                    console.log(response)
                    if(response.result) {
                        localStorage.removeItem('cart')
                        document.getElementById('loader').classList.remove('d-none')
                        document.getElementById('payment_box').classList.add('d-none')

                        setTimeout(function () {
                            document.getElementById('final_modal').click()
                            setTimeout(function () {
                                location.href = '{{ route('home') }}'
                            }, 6000)
                        }, 5000)

                    }
                    else{
                        for(let err in response.errors) {
                            setInvalid(err, response.errors[err][0])
                        }
                    }
                })
        }


        function clearInvalid() {
            document.getElementById('card_number').classList.remove('is-invalid')
            document.getElementById('cvv').classList.remove('is-invalid')
            document.getElementById('name').classList.remove('is-invalid')
            document.getElementById('exp_date').classList.remove('is-invalid')
        }


        function setInvalid(id, err) {
            document.getElementById(id).classList.add('is-invalid')
            document.getElementById(id + '_error').innerText = err
        }

        window.onload = function () {
            setTimeout(function () {
                document.getElementById('loader').classList.add('d-none')
                document.getElementById('payment_box').classList.remove('d-none')
            }, 3000)
        }

        function doFormat(x, pattern, mask) {
            var strippedValue = x.replace(/[^0-9]/g, "");
            var chars = strippedValue.split('');
            var count = 0;

            var formatted = '';
            for (var i=0; i<pattern.length; i++) {
                const c = pattern[i];
                if (chars[count]) {
                    if (/\*/.test(c)) {
                        formatted += chars[count];
                        count++;
                    } else {
                        formatted += c;
                    }
                } else if (mask) {
                    if (mask.split('')[i])
                        formatted += mask.split('')[i];
                }
            }
            return formatted;
        }

        document.querySelectorAll('[data-mask]').forEach(function(e) {
            function format(elem) {
                const val = doFormat(elem.value, elem.getAttribute('data-format'));
                elem.value = doFormat(elem.value, elem.getAttribute('data-format'), elem.getAttribute('data-mask'));

                if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', val.length);
                    range.select();
                } else if (elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(val.length, val.length);
                }
            }
            e.addEventListener('keyup', function() {
                format(e);
            });
            e.addEventListener('keydown', function() {
                format(e);
            });
            format(e)
        });
    </script>
</body>
</html>
